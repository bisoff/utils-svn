#### SVN tools

- sek/sok 	- for rapid update/commit whole SVN repo (used in button bar in Total Commander)
- svnsw		- switch svn version by renaming folder if you use some versions of CLI svn.
		  ex. old version of CLI svn that compatible with GUI client, like SmartSVN and last CLI version of svn for new projects/repos
		  this is more convenient than replace PATH var, imho
